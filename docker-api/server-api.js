const fs = require('fs')
const bodyParser = require('body-parser')
const jsonServer = require('json-server')

const server = jsonServer.create()
const router = jsonServer.router('./database.json')

//server.use(bodyParser.urlencoded({extended: true}))
//server.use(bodyParser.json())
server.use(jsonServer.defaults());

server.use(router)

const port_num = 80;
server.listen(port_num, '0.0.0.0', () => {
  console.log('Run API Server on port '+port_num)
})
